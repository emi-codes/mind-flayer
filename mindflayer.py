from cortex2 import EmotivCortex2Client
import time
import tkinter as tk
import threading
import queue as Queue
import os

url = "wss://localhost:6868"
counter = 0

class GUI:
    def __init__(self, master):
        self.master = master
        # self.start_button = tk.Button(self.master, command=self.tb_click)
        # self.start_button.configure(
        #     text="Start",
        #     padx=50
        #     )
        # self.start_button.pack(side=tk.TOP)
        self.close_button = tk.Button(self.master, command=self.quitThread)
        self.close_button.configure(
            text="Beenden",
            padx=50
            )
        self.close_button.pack(side=tk.TOP)
        self.canvas = tk.Canvas(self.master, width=500, height=400)
        self.canvas.configure()
        self.canvas.pack(
            padx=50
        )
        self.a = self.canvas.create_rectangle(20, 20, 200, 200, fill="red")
        self.master.after(1, self.bciauth())
        self.master.after(2, self.bcisubs())
        self.master.after(3, self.bcithread())
    
    def quitThread(self):        
        os._exit(0) #close everything, all threads and GUI
        
    # def tb_click(self):        
    #     #self.bcithread()
    #     #self.queue = Queue.Queue()
    #     #ThreadedTask(self.queue).start()
    #     self.master.after(100, self.process_queue)

    def bciauth(self):
        # Remember to start the Emotiv App before you start!
        # Start client with authentication
        global client 
        # Have to register the app in Account Information at emotiv.com to get client ID and secret
        client = EmotivCortex2Client(url,
                                    client_id='YOUR_CLIENT_ID',
                                    client_secret='YOUR_CLIENT_SECRET',
                                    check_response=True,
                                    authenticate=True,
                                    debug=False)

        # Get User Data
        print(client.get_user_login())

        # Test API connection by using the request access method
        client.request_access()

        # Explicit call to Authenticate (approve and get Cortex Token)
        client.authenticate()
        client.connect_headset(0)
        client.create_session(0)
        #bcithread()

    def bcithread(self):
        global t
        t = threading.Thread(target = self.bciloop)
        t.daemon = True
        t.start()

    def printtest(self):
        print("Hello world")
        for thread in threading.enumerate(): 
            print(thread.name)
        root.after(100,self.printtest)

    def bcisubs(self):
        # Subscribe to the motion and mental command streams
        # Spins up a separate subscription thread
        client.subscribe(streams=["fac", "com"]) 
        print("Subscriber thread is: ", client.subscriber_thread)
        # Test message handling speed
        # a = client.subscriber_messages_handled
        # time.sleep(5)
        # b = client.subscriber_messages_handled
        # print((b - a) / 5)

        # Grab a single instance of data
        #print("Receive data: ", client.receive_data())

        # Connect to headset, connect to the first one found, and start a session for it
        #print(client.query_headsets())
        

    # Continously grab data, while making requests periodically
    def bciloop(self): 
        # for thread in threading.enumerate(): 
        #     print(thread.name)       
        # print("Hi")
        global counter
        counter += 1
        time.sleep(1) # output time
        # if (counter == 5):
        #     self.close_button.invoke()
        if counter % 5000 == 0:
            print(client.request_access())

        # Try stopping the subscriber thread
        if counter == 50000:
            client.stop_subscriber()
            #break

        # output:
        try:
            # Check the latest data point from the motion stream, from the first session
            # print(list(client.data_streams.values())[0]['fac'][0] # original
            action1 = list(client.data_streams.values())[0]['fac'][0]['data'][0] # get eye action, e.g. blink
            action2 = list(client.data_streams.values())[0]['com'][0]['data'][0] # get action of mental command, e.g. push
            print(action1)
            if (action1 == 'blink'):
                print ("I blinked!")
                self.close_button.invoke()
            if (action2 == 'push'):
                print ("I pushed!")
            if (action2 == 'pull'):
                print ("I pulled!")
                self.canvas.coords(self.a, 20, 20, 100, 100 )
        except:
            pass
        root.after(100,self.bcithread) # infinite loop instead of while True

    # def process_queue(self):
    #     try:
    #         msg = self.queue.get(0)
    #         # Show result of the task if needed
    #         client.stop_subscriber()
    #     except Queue.Empty:
    #         self.master.after(100, self.process_queue)


class ThreadedTask(threading.Thread):
    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue

    def run(self):
        # time.sleep(5)  # Simulate long running process
        self.queue.put("Task finished")

    
root = tk.Tk()
root.title("Mind Flayer")
root.geometry("500x500")
main_ui = GUI(root)
root.mainloop()

